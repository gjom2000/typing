using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject setting;
    private bool gameDifficulty;
    private bool isPause;

    private void Awake()
    {
        isPause = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SetPause(!isPause);
        }
    }

    private void SetPause(bool _isPause)
    {
        isPause = _isPause;

        setting.SetActive(isPause);
        KeyManager.instance.isPause = isPause;
    }

    public void SetDifficulty(bool _Easy)
    {
        KeyManager.instance.SetDifficulty(_Easy);
        SetPause(false);
    }
}
