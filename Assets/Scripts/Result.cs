using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Result : MonoBehaviour
{
    [SerializeField] private GameObject Success;
    [SerializeField] private GameObject Fail;

    private void Awake()
    {
        Success.SetActive(false);
        Fail.SetActive(false);
    }

    public void SetResult(bool _Success)
    {
        Success.SetActive(_Success);
        Fail.SetActive(!_Success);
    }
}
