using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class KeyManager : MonoBehaviour
{
    static public KeyManager instance;

    public bool isPause;

    public GameObject keyPrefab;
    [SerializeField] private GameObject keyParent;

    private int gameLevel;
    [SerializeField]private KeyItem[] keys;

    private int Iterator;

    private float maxRestartCount;
    private float restartCount;
    private bool canTry;

    [SerializeField] private Slider progress;
    [SerializeField] private TextMeshProUGUI timer;
    [SerializeField] private Result result;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip[] audioClips;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        //keys = new KeyItem[20];
        canTry = true;
        isPause = false;

        // Normal
        gameLevel = 7;
        maxRestartCount = 5.0f;

        // Hard
        //gameLevel = 8;
        //maxRestartCount = 2.0f;

        progress.maxValue = maxRestartCount - 1.0f;
        Iterator = 0;

        //for (int idx = 0; idx < 20; idx++)
        //{
        //    GameObject obj = Instantiate(keyPrefab);
        //    obj.transform.SetParent(keyParent.transform);
        //    obj.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        //    obj.SetActive(false);
        //    keys[idx] = obj.GetComponent<KeyItem>();
        //}
        SortingHorizontal();
    }

    private void Update()
    {
        if (isPause)
            return;

        restartCount -= Time.deltaTime;
        progress.value = restartCount - 1.0f;
        timer.SetText(progress.value.ToString("N1"));
        if (canTry && restartCount <= 1.0f)
        {
            SetResult(false);
        }
        if (restartCount <= 0)
        {
            Reset();
            return;
        }

        if (canTry == false)
            return;

        int val = -1;

        foreach (KeyCode item in Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKeyDown(item))
            {
                switch (item)
                {
                    case KeyCode.Q:
                        val = 0;
                        break;

                    case KeyCode.W:
                        val = 1;
                        break;

                    case KeyCode.E:
                        val = 2;
                        break;

                    case KeyCode.R:
                        val = 3;
                        break;

                    case KeyCode.A:
                        val = 4;
                        break;

                    case KeyCode.S:
                        val = 5;
                        break;

                    case KeyCode.D:
                        val = 6;
                        break;

                    case KeyCode.F:
                        val = 7;
                        break;

                    default:
                        break;
                }
            }
        }

        if (val == -1)
            return;

        if (keys[Iterator].IsCorrect(val))
        {
            // Success
            PlaySound(0);
            keys[Iterator].SetState(2);
            if (Iterator < gameLevel - 1)
            {
                keys[++Iterator].SetState(1);
            }
            else
            {
                SetResult(true);
            }
        }
        else
        {
            // Fail
            SetResult(false);
        }
    }

    public void SetDifficulty(bool _Easy)
    {
        if (_Easy)
        {
            // Normal
            gameLevel = 7;
            maxRestartCount = 5.0f;
        }
        else
        {
            // Hard
            gameLevel = 8;
            maxRestartCount = 2.0f;
        }

        Reset();
    }

    private void SetResult(bool _Success)
    {
        if (_Success)
            PlaySound(2);
        else
            PlaySound(1);

        keyParent.SetActive(false);

        restartCount = Mathf.Clamp(restartCount, 0, 1.0f);
        canTry = false;

        result.SetResult(_Success);
        result.gameObject.SetActive(true);
    }

    public void Reset()
    {
        keyParent.SetActive(true);
        result.gameObject.SetActive(false);

        restartCount = maxRestartCount;
        progress.maxValue = maxRestartCount - 1.0f;
        Iterator = 0;
        canTry = true;
        foreach (var item in keys)
        {
            item.Reset();
            item.gameObject.SetActive(false);
        }
        SortingHorizontal();
    }

    private void SortingHorizontal()
    {
        int startX = (gameLevel - 1) * (-50);
        for (int idx = 0; idx < gameLevel; idx++)
        {
            keys[idx].Initialize(startX + (idx * 100));
        }
        keys[0].SetState(1);
    }

    private void PlaySound(int _idx)
    {
        audioSource.clip = audioClips[_idx];
        audioSource.Play();
    }
}
