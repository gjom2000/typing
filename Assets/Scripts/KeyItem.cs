using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyItem : MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private RectTransform rectTransform;

    [SerializeField] private Sprite[] sprites;

    private int nowKey;

    private void Awake()
    {
        //rectTransform = GetComponent<RectTransform>();
        //image = GetComponent<Image>();
    }

    public void Reset()
    {
        image.color = Color.white;
        rectTransform.anchoredPosition = Vector2.zero;
        nowKey = -1;
        SetState();
    }

    public void Initialize(int _x)
    {
        nowKey = Random.Range(0, 8);
        image.sprite = sprites[nowKey];
        rectTransform.anchoredPosition = new Vector2(_x, 0);
        gameObject.SetActive(true);
    }

    public bool IsCorrect(int _val)
    {
        return nowKey == _val;
    }

    public void SetState(int _state = 0)
    {
        switch (_state)
        {
            case 0:
                {
                    // None
                    image.color = Color.white;
                    rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, 0);
                    break;
                }
            case 1:
                {
                    // Hilight
                    image.color = new Color(1.0f, 0.9f, 0.4f);
                    rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, 20);
                    break;
                }
            case 2:
                {
                    // Clear
                    image.color = new Color(0.5f, 0.5f, 0.5f);
                    rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, 0);
                    break;
                }
            default:
                {
                    break;
                }
        }
    }
}
